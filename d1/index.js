let grades = [98.5, 94.3, 89.2, 90.1];
console.log(grades);

/* 
    Array
        - used to store multiple related values in a single variable
        - provide access to a number of functions/methods that help in achieving tasks that we can perform on the elements inside the arrays
    
    SYNTAX:
        let/const arrayName = [elementA, elementB, elementC, ..., elementN];
*/
// it is important that we store RELATED values inside an array so that its variable can live up to its description of its value
let computerBrands = ["Lenovo", "Dell", "Asus", "HP", "MSI", "Acer", "CDR-King"];
console.log(computerBrands);

// using unrelated data types in a single array is not recommended
let mixedArr = [12, "Asus", null, undefined];
console.log(mixedArr);

// Reassigning of array values
console.log("Array before reassigning: " + mixedArr);
//reassigning elements of an array can be done on const arrays
mixedArr[0] = "Hello World";
console.log("Array after reassigning: " + mixedArr);
console.log(mixedArr);

// SECTION - Reading from Arrays
/* 
    - can access array elements through array indeces
        - index - term used to declare the position of an element in an array
        - in JS, the first element is associated with number 0, incrementing as the number of the elements increase

        SYNTAXT:
            ArrayName[index];
*/
console.log(grades);
console.log(grades[0]);
console.log(computerBrands.length);
// accessing an index that is not existing would return undefined because this means that the element is not there
console.log(computerBrands[7]);
// one of the common uses of length property
if (computerBrands.length > 5){
    console.log("We have too many suppliers. Please coordinate with the operations manager.")
}
// Since the first element of an array starts at 0, subtracting 1 from the length will offset the value by 1 allowing us to get the last index of the array, in case we forgot the number of elements
let lastElementIndex = computerBrands.length - 1;
console.log(computerBrands[lastElementIndex]);

// Section - ARRAY METHODS
// methods in arrays are similar to functions, these methods can be done in array and objects aalone
// Mutator Methods

/* 
   - functions that mutate or change/alter an array after they have been created
   - will manipulate the original array performing various tasks such as adding and removing elements
*/

let fruits = ["Apple", "Mango", "Rambutan", "Lanzones", "Durian"];
console.log(fruits);

// push() - adds an element at the end of an array
/* 
    SYNTAX:
        arrayName.push(newElement);
*/
// can also accept multiple arguments
let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);

console.log("Mutated array from push method: ");
console.log(fruits);

// pop()
/* 
    - remove the last element of an array
    SYNTAX:
        arrayName.pop()
*/
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from push method: ");
console.log(fruits);

// unshift()
/* 
    - works like push(), but adds the new element/s at the start of the array
    SYNTAX:
        arrayName.unshift(newElement);
*/
fruits.unshift("Banana");
fruits.unshift("Lime");
console.log("Mutated array from Unshift method: ");
console.log(fruits);

// shift()
/* 
    - removes the first element of an array and returns said element
    SYNTAX:
        arrayName.shift();
*/
let spoiledFruits = fruits.shift();
console.log("Mutated array from Shift method: ");
console.log(fruits);

spoiledFruits = fruits.shift();
console.log("Mutated array from Shift method: ");
console.log(fruits);

// splice(startingIndex, limit, newItem1, newItemN)
/* 
    - simultaneously removes and adds elements from specified index number
    SYNTAX:
        arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/
fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated array from Splice method: ");
console.log(fruits);

// sort()
/* 
    - rearranges the elements in an array in alphanumeric order
    SYNTAX:
        arrayName.sort();
*/
fruits.sort();
console.log("Mutated array from Sort method: ");
console.log(fruits);

// reverse()
/* 
    - reverse the order of the array
    SYNTAX:
        arrayName.reverse();
*/
fruits.reverse();
console.log("Mutated array from Sort method: ");
console.log(fruits);

// NON-Mutator Methods
/* 
    - functions that do not modify the original array
    - do not manipulate the elements inside the array even they are performing tasks such as returning elements from an array and combining them with other arrays and printing the output
*/
let countries = ["RUS", "CH", "JPN", "PH", "KOR", "AUS", "CAN", "PH" ];

// indexOf()
/* 
    - returns the index of the first matching element found in an array
    - matches from the first element to the last element
    - returns -1 if element is non-existing
    SYNTAX:
        arrayName.indexOf(searchValue)
*/
let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf: " + firstIndex);
firstIndex = countries.indexOf("KAZ");
console.log("Result of indexOf: " + firstIndex);

// lastIndexOf()
/* 
    - returns the index of the last matching element found in an array
    - matches from the last element to the first element
    - returns -1 if element is non-existing
    SYNTAX:
        arrayName.indexOf(searchValue)
*/

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf: " + lastIndex);
lastIndex = countries.lastIndexOf("KAZ");
console.log("Result of lastIndexOf: " + firstIndex);

// slice()
/* 
    - copies a part of an array and creates a new array
    SYNTAX:
        arrayName.slice(startingIndex)
        arrayName.slice(startingIndex, endingIndex)
*/
let slicedArrayA = countries.slice(2);
console.log("Result of slice: " + slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log("Result of slice: " + slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log("Result of slice: " + slicedArrayC);

// toString()
/* 
    - returns the array as a string, separated by commas
    SYNTAX:
        arrayName.toString();
*/
let stringArray = countries.toString();
console.log("Result of toString: ");
console.log(stringArray);
console.log(countries);

// concat()
/* 
    - used to combine two arrays and returns the combined result
    SYNTAX:
        arrayA.concat(arrayB);
        arrayA.concat(arrayB, arrayC, ...arrayN);
*/
let tasksA = ["drink HTML", "eat JavaScript"];
let tasksB = ["inhale CSS", "breathe SASS"];
let tasksC = ["get GIT", "be node"];

let tasks = tasksA.concat(tasksB);
console.log("Result of concat: ");
console.log(tasks);

// combining multiple arrays
let allTasks = tasksA.concat(tasksB, tasksC);
console.log("Result of concat: ");
console.log(allTasks);

// join()
/* 
    - returns an array as string separated by specified string separator
    SYNTAX:
        arrayName.join('separator')
*/
let users = ["John", "Jane", "Joe", "Jobert", "Julius"];
console.log(users.join());
console.log(users.join(' '));
console.log(users.join('-'));

// Iterator methods
/* 
    - iteration methods are loops designed to perform repetitive tasks on arrays
    - useful for manipulating array data resulting in complex tasks
*/

// forEach()
/* 
    - similar to a for loop that loops through all elements
    - variable names for arrays are usually written in plural form of the data stored in an array
    - it's common practice to use the singular form of the array for parameter names used in array loops
    - array iterations normally work with a function supplied as an argument
    - how these functions works is by performing tasks that are pre-defined within the array's method
    SYNTAX:
        arrayName.forEach(function(element){
            statement/conditions
        })
*/
allTasks.forEach(function(task){
    console.log(task);
})

// forEach() with conditional statements
console.log("forEach: ");

let filteredTasks = [];
allTasks.forEach(function(task){
    if (task.length > 10) {
        // we stored the filtered elements inside another variable to avoid confusion should we need the original array intact
        filteredTasks.push(task);
        console.log(task.length);
    }
});
console.log("Result of forEach: ");
console.log(filteredTasks);

// map()
/* 
    - iterates on each element and returns a new array with different values depending on the result of the function's operation
    - useful for tasks where mutating/changing the elements are required
    SYNTAX:
        let/const resultArray = arrayName.map(function(element){
            return statement
        })

        or 
        let/const resultArray = arrayName.map(element => return statement})

*/
let numbers = [1, 2, 3, 4, 5];

// let numbersMap = numbers.map(function(number){
//     return number * number;
// })
//shorthand version
let numbersMap = numbers.map(number => number * number);
console.log("Result of map: ");
console.log(numbersMap);

// every()
/* 
    - checks if all elements pass the given condition
    - returns a boolean data type depending if all elements meet the condition (true) or not (false)
    SYNTAX:
        let/const result = arrayName.every(function(element){
            return expression/condition
        })
*/
let allValid = numbers.every(number => number < 3);
console.log("Result of every(): ");
console.log(allValid);

// some()
/* 
    - checks if atleast one element in the array passes the given condition
    - returns boolean depending on the result of the function
    SYNTAX:
        let/const result = arrayName.some(function(element){
            return expression/condition
        })
*/
let someValid = numbers.some(number => number < 2);
console.log("Result of some(): ");
console.log(someValid);

// filter()
/* 
    - returns a new array that contains copies of the elements which meets the specified condition
    - returns an empty array if no element/s passed the specified condition
    SYNTAX:
    let/const result = arrayName.filter(function(element){
            return expression/condition
        })
*/
let filterValid = numbers.filter(number => number < 3);
console.log("Result of filter(): ");
console.log(filterValid);

let parts = ["Mouse", "Keyboard", "Unit", "Monitor"];
console.log(parts);

// reduce()
/* 
    - evaluates elements from left to right, and returns/reduces the array into a single value
    SYNTAX:
        let/const result = arrayName.reduce(function(accumulator, currentValue){
            return statement/operation;
        })
    
    - accumulator - stores the result for every iteration
    - currentValue - the next/current element in the array that is evaluated in each iteration
*/
let iteration = 0;
// let reducedArray = numbers.reduce((x,y) => x + y);
let reducedArray = numbers.reduce(function(x,y){
    console.warn(++iteration);
    console.log(x);
    console.log(y);
    return x + y;
});
console.log(numbers);
console.log(reducedArray);

// when used in strings, returns the concatinated strings of the array
//let reducedParts = parts.reduce((x,y) => x + y);
let reducedParts = parts.reduce(function(x,y){
    console.warn(++iteration);
    console.log(x);
    console.log(y);
    return x + y;
});
console.log(reducedParts);

// includes method
/* 
    - searches each element of an array if they have the specific character that is inside its arguments
    - can only accept one argument
*/
let filteredParts = parts.filter(function(part){
    return part.toLowerCase().includes("i");
})
console.log(filteredParts);