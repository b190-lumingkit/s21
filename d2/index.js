let cities = ["Tokyo", "Manila", "Seoul", "Jakarta", "Sim"];
console.log(cities);
console.log(cities[cities.length - 1]);

blankArr = [];
console.log(blankArr); // returns empty array
console.log(blankArr.length); // returns 0
console.log(blankArr[blankArr.length]); // returns undefined

console.log(cities);
// decrement operator can be used to delete the last element of an array using .length property
cities.length --;
console.log(cities);

let lakersLegends = ["Kobe", "Shaq", "Magic", "Kareem", "LeBron"];
console.log(lakersLegends);

lakersLegends.length++;
console.log(lakersLegends);

lakersLegends[lakersLegends.length - 1] = "Pau Gasol";
console.log(lakersLegends);

// use the .length property to directly add elements at the ened of the array;
lakersLegends[lakersLegends.length] = "Anthony Davis";
console.log(lakersLegends);

let numArray = [5, 12, 30, 46, 40];
for (let index = 0; index < numArray.length; index++) {
    if (numArray[index] % 5 === 0) {
        console.log(numArray[index] + " is divisible by 5");
    } else {
        console.log(numArray[index] + " is not divisible by 5");
    }
}

for (let i = 0; i < numArray.length; i++) {
    console.log(numArray[i]);
}

console.log("Reverse Loop")
for (let i = numArray.length -1; i >= 0; i--) {
    console.log(numArray[i]);
}



// multidimensional arrays
/* 
    - are useful for storing complex data structures
    - a practical application of this is to help visualize/create real world objects
    - though useful in a number of cases, creating complex arrays is not always recommended
*/
let chessBoard = [
    ["a1", "b1", "c1", "d1", "e1"],
    ["a2", "b2", "c2", "d2", "e2"],
    ["a3", "b3", "c3", "d3", "e3"],
    ["a4", "b4", "c4", "d4", "e4"],
    ["a5", "b5", "c5", "d5", "e5"]
];
console.log(chessBoard);
//accessing multidimensional array
console.log(chessBoard[1][4]);
